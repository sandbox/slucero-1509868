This module allows content types to be configured to add a menu link by default with an additional option for this menu link to be hidden by default. This is useful for managing site structure through menus without the content administrator having to manage menu placement.

This module also helps to solve the problem of having the correct item in a menu tree expanded without having to show the final node in the menu.
